//
//  UIAlertController+Extension.swift
//  NetworkManager
//
//  Created by Muhammad Umer Farooq on 3/18/18.
//  Copyright © 2018 Muhammad Umer Farooq. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

private var nvActivityIndicatorColor = UIColor.white
private var nvActivityIndicatorStyle = NVActivityIndicatorType.ballTrianglePath

public var NVActivityIndicatorColor: UIColor {
    
    get {
        return nvActivityIndicatorColor
    }
    
    set(newValue) {
        nvActivityIndicatorColor = newValue
    }
}
public var NVActivityIndicatorStyle: NVActivityIndicatorType {
    
    get {
        return nvActivityIndicatorStyle
    }
    set(newValue) {
        nvActivityIndicatorStyle = newValue
    }
}

extension UIAlertController {
    
    /// Function for creating a alert with no title, a default Ok action that dismisses the alert and a specified message
    ///
    /// - Parameter message: the alert message
    /// - Returns: a configured UIAlertController
    public static func alertWithMessage(message: String) -> UIAlertController {
        /// This function was initially a convenience init but the alert was being deallocated while it was
        ///  being presenting for no reason. Creating this static function fixed the issue.
        
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        return alert
    }
    
    /// Function for creating an alert with no title, a specified message and a custom Ok action
    ///
    /// - Parameters:
    ///   - message: the alert message
    ///   - okAction: action to be performed on Ok button
    /// - Returns: a configured UIAlertController
    public static func alert(withMessage message: String,
                      andAction okAction: UIAlertAction) -> UIAlertController {
        /// This function was initially a convenience init but the alert was being deallocated while it was
        ///  being presenting for no reason. Creating this static function fixed the issue.
        
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alert.addAction(okAction)
        return alert
    }
    
    /// Function for creating an alert with title and two buttons. Ok button
    /// completion handler will be provided too
    ///
    /// - Parameters:
    ///   - message: alert message
    ///   - okAction: Action to be performed on Click of Ok button
    ///   - cancelTitle: title for cancel Button
    /// - Returns: a configured UIAlertController
    public static func alertWithButtons(message: String, okAction: UIAlertAction,
                                 cancelTitle: String) -> UIAlertController {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: cancelTitle, style: .cancel, handler: nil)
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        return alert
    }
    
    /// Function for creating an alert with two buttons placed vertically. This is majorly used for image picker options in this project.
    ///
    /// - parameter title:              alert title
    /// - parameter photoLibraryAction: Action to be performed on Click of Photo Library button
    /// - parameter cameraAction:       Action to be performed on Click of Camera button
    ///
    /// - returns: a configured UIAlertController
    public static func alertForImagePicker(title: String, photoLibraryAction: UIAlertAction,
                                    cameraAction: UIAlertAction) -> UIAlertController {
        let actionSheetController = UIAlertController(title: title, message: nil, preferredStyle: .actionSheet)
        
        actionSheetController.addAction(photoLibraryAction)
        actionSheetController.addAction(cameraAction)
        
        let cancelActionButton: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
        }
        actionSheetController.addAction(cancelActionButton)
        
        return actionSheetController
    }
    
    public static func alertForMultipleOptions(title: String, actions: [UIAlertAction]) -> UIAlertController {
        let alert = UIAlertController(title: title, message: nil, preferredStyle: .actionSheet)
        for action in actions {
            alert.addAction(action)
        }
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: "Cancel"), style: .cancel, handler: nil)
        alert.addAction(cancelAction)
        
        return alert
    }
}


// MARK: - Alert Display
extension UIViewController {
    
    /// Function for displaying a Alert Message inside the view controller
    ///
    /// - Parameter message: String representing the alert message
    public func showAlert(withMessage message: String) {
        DispatchQueue.main.async(execute: {[unowned self] in
            self.present(UIAlertController.alertWithMessage(message: message),
                         animated: true, completion: nil)
        })
    }
    
    /// Function for displaying Alert message along with customized action on click of ok button
    ///
    /// - Parameters:
    ///   - message: string representing alert message
    ///   - action: action to be performed on ok button
    public func showAlert(withMessage message: String,
                   andAction action: UIAlertAction) {
        DispatchQueue.main.async(execute: {[unowned self] in
            self.present(UIAlertController.alert(withMessage: message,
                                                 andAction: action),
                         animated: true, completion: nil)
        })
    }
}

extension UIViewController: NVActivityIndicatorViewable {
    
    public func showAnimation() {
        DispatchQueue.main.async {
            let size = CGSize(width: 50, height:50)
            let color = nvActivityIndicatorColor
            self.startAnimating(size, message: "", type: nvActivityIndicatorStyle, color: color)
        }
    }
    
    public func hideAnimation() {
        DispatchQueue.main.async {
            self.stopAnimating()
        }
    }
}

