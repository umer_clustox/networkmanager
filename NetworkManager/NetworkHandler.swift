//
//  NetworkHandler.swift
//  NetworkManager
//
//  Created by Muhammad Umer Farooq on 3/17/18.
//  Copyright © 2018 Muhammad Umer Farooq. All rights reserved.
//

import Alamofire

public struct NetworkHandler {
    
    public init() {
        
    }
    
    public typealias ResponseHandler = (_ response: DataResponse<Any>?, _ error: Error?) -> ()
    
    public func requestForPost(method: HTTPMethod,
                               headers: [String: String]? = nil,
                               params: [String: Any]? = nil ,
                               url: URL,
                               completion: ResponseHandler? = nil) {
        
        Alamofire.request(url, method: method, parameters: params, encoding: JSONEncoding.default, headers: headers).validate().responseJSON { response in
            switch response.result {
            case .success:
                completion?(response, nil)
            case .failure(let error):
                completion?(response, error)
            }
        }
        
    }
    
    public func requestForGET(headers: [String: String]? = nil,
                              params: [String: Any]? = nil ,
                              url: URL,
                              completion: ResponseHandler? = nil) {
        
        Alamofire.request(url, method: .get, parameters: params, encoding: URLEncoding.default, headers: headers).validate().responseJSON { response in
            switch response.result {
            case .success:
                completion?(response, nil)
            case .failure(let error):
                completion?(response, error)
            }
        }
    }
}

