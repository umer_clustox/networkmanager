//
//  JSONUtilities.swift
//  NetworkManager
//
//  Created by Muhammad Umer Farooq on 3/18/18.
//  Copyright © 2018 Muhammad Umer Farooq. All rights reserved.
//

import UIKit


public protocol LocalizedStringConvertible {
    /// Localized string representation
    var localizedString: String { get }
}

// MARK: - Types
public typealias JSON = Any
public typealias JSONDictionary = [String: JSON]
public typealias JSONArray = [JSON]

// MARK: - Casting Functions

/**
 Casts a JSON value into an optional String if possible. If input parameter is
 null, it will return empty string
 
 - parameter element: An optional JSON value
 
 - returns: A String value if element is null or nil, it will return empty
 string
 */
public func JSONToStringIfNull(element: JSON?) -> String? {
    guard let elementString = element as? String else {
        return ""
    }
    return elementString
}

/**
 Casts a JSON value into an optional String if possible.
 
 - parameter element: An optional JSON value
 
 - returns: An optional String value
 */
public func JSONToString(element: JSON?) -> String? {
    return element as? String
}


/// Casts a JSON value into an optional Int if possible. If input parameter is null,
/// it will return 0
///
/// - Parameter element: An optional JSON value
/// - Returns: An optional Int value
public func JSONToIntIfNull(element: JSON?) -> Int? {
    guard let elementInt = element as? Int else {
        return 0
    }
    return elementInt
}

/**
 Casts a JSON value into an optional Int if possible.
 
 - parameter element: An optional JSON value
 
 - returns: An optional Int value
 */
public func JSONToInt(element: JSON?) -> Int? {
    return element as? Int
}

/**
 Casts a JSON value into an optional Int64 if possible.
 
 - parameter element: An optional JSON value
 
 - returns: An optional Int64 value
 */
public func JSONToInt64(element: JSON?) -> Int64? {
    return element as? Int64
}

/**
 Casts a JSON value into an optional Double if possible.
 
 - parameter element: An optional JSON value
 
 - returns: An optional Double value
 */
public func JSONToDouble(element: JSON?) -> Double? {
    return element as? Double
}

/**
 Casts a JSON value into an optional Bool if possible.
 
 - parameter element: An optional JSON value
 
 - returns: An optional Bool value
 */
public func JSONToBool(element: JSON?) -> Bool? {
    return element as? Bool
}

/**
 Casts a JSON value into an optional JSONDictionary type if possible.
 
 - parameter element: An optional JSON value
 
 - returns: An optional JSONDictionary value
 */
public func JSONToDictionary(element: JSON?) -> JSONDictionary? {
    return element as? JSONDictionary
}

/**
 Casts a JSON value into an optional JSONArray type if possible.
 
 - parameter element: An optional JSON value
 
 - returns: An optional JSONArray value
 */
public func JSONToArray(element: JSON?) -> JSONArray? {
    return element as? JSONArray
}

/**
 Casts a JSON value into an optional array of T values that implement JSONDecodable.
 
 ## Usage: ##
 
 if let myItemArray: [Item] = JSONToArray(dictionary["items"] {
 ...
 }
 
 - parameter element: An optional JSON value
 
 - returns: An optional array of T values
 */
public func JSONToArray<T: JSONDecodable>(element: JSON?) -> [T]? where T.T == T {
    if let array = JSONToArray(element: element) {
        var results = [T]()
        for arrayObject in array {
            if let item  = T.decode(JSON: arrayObject) {
                results.append(item)
            }
        }
        return results
    }
    return nil
}

/**
 Casts a JSON value into an optional NSDate type if possible.
 The JSON value is parsed with the supplied dateFormatter instance.
 
 - parameter element: An optional JSON value
 - parameter dateFormatter: A DateFormatter instance
 
 - returns: An optional NSDate value
 */
public func JSONToDate(element: JSON?, dateFormatter: DateFormatter) -> Date? {
    if let string = JSONToString(element: element) {
        return dateFormatter.date(from: string)
    }
    return nil
}

/**
 Casts a JSON value into an optional NSURL type if possible.
 
 - parameter element: An optional JSON value
 
 - returns: An optional NSURL value
 */
public func JSONToURL(element: JSON?) -> NSURL? {
    if let URLString = JSONToString(element: element) {
        return NSURL(string: URLString)
    }
    return nil
}

public func generateError(statusCode: Int,description: String) -> NSError {
    
    let error = NSError(domain: "", code: statusCode, userInfo: ["localizedDescription":description])
    return error
}




// MARK: - Protocols

/**
 A protocol that allows any custom type to be decoded and instanciated
 from an optional JSON value.
 */
public protocol JSONDecodable {
    associatedtype T
    
    /**
     Function used to decode a JSON object to a generic inferred type
     
     - parameter element: the JSON object
     - returns: a deserialised JSON object in case of success, otherwise nil
     */
    static func decode(JSON element: JSON?) -> T?
}

/**
 A protocol that allows any custom type to be encoded into
 an optional JSON (or JSONDictionary) type.
 */
public protocol JSONEncodable {
    associatedtype T
    
    /**
     Function used to encode a generic inferred type to a JSON object
     
     - parameter JSONObject: the object that will be encoded
     - returns: a JSON object in case of success, otherwise nil
     */
    static func encode(JSONObject: T?) -> JSON?
}

/**
 Casts a JSON value into an optional NSDecimalNumber if possible.
 
 - parameter element: An optional JSON value
 
 - returns: An optional NSDecimalNumber value
 */
public func JSONToDecimalNumber(element: JSON?) -> NSDecimalNumber? {
    guard let value = element as? NSNumber else { return nil }
    
    let decimalValue = value.decimalValue
    
    return NSDecimalNumber(decimal: decimalValue)
}

/**
 JSON Errors
 
 - UnableToDecode: Unable to decode JSON
 - UnableToEncode: Unable in encode to JSON
 */
public enum JSONError: Error {
    case UnableToDecode
    case UnableToEncode
}

extension JSONError: LocalizedStringConvertible {
    /// Localized String Representation
    public var localizedString: String {
        switch self {
        case .UnableToDecode: return NSLocalizedString("Unable to decode JSON", comment: "Unable to decode JSON")
        case .UnableToEncode: return NSLocalizedString("Unable to encode in JSON", comment: "Unable to encode in JSON")
        }
    }
}


public var screenHeight: CGFloat {
    return UIScreen.main.bounds.height
}

public var screenWidth: CGFloat {
    return UIScreen.main.bounds.width
}
