
### Installation ###

To integrate NetworkManager into your Xcode project using CocoaPods, specify it in your Podfile:

    platform :ios, '10.0'
    
    source 'https://github.com/CocoaPods/Specs.git'
    source 'https://umer_clustox@bitbucket.org/umer_clustox/networkmanagerpodspecs.git'
    
    use_frameworks!

    target '<Your Target Name>' do
        pod 'NetworkManager', '~> 0.5.0'
    end
    
Then, run the following command:

    $ pod install
